#!/bin/sh

CFSSL_MISSING=0
SCRIPT_DIR="$( cd "$( dirname "${0}" )" && pwd )"

_dl() {
  if ! which cfssl > /dev/null || [ "$(sha256sum $(which ${1}) | awk '{print $1}')" != "${2}" ]
  then
    CFSSL_MISSING=1
    echo "Downloading ${1}"
    curl -Ls -o ${1} https://github.com/cloudflare/cfssl/releases/download/v1.4.1/${1}_1.4.1_linux_amd64
    if [ "$(sha256sum ${1} | awk '{print $1}')" = "${2}" ]
    then
      chmod +x ${1}
      echo "${1} binary has been downloaded here: ${SCRIPT_DIR}/${1}"
      echo "Please put it in your PATH"
    else
      echo "There was a problem while downloading ${1} binary. Bad sha256sum"
    fi
  fi
}

_dl cfssl d01a26bc88851aab4c986e820e7b3885cedf1316a9c26a98fbba83105cfd7b87
_dl cfssljson 05d67e05cacb8b2e78e737637acdcf9127b0732f0c4104403e9e9b74032fd685

if [ $CFSSL_MISSING -gt 0 ]
then
  exit 1
fi
echo '{"CA":{"expiry": "127200h"},"CN":"CA","key":{"algo":"rsa","size":4096}}' | cfssl gencert -initca - | cfssljson -bare ca -
