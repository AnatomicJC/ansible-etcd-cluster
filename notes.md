https://etcd.io/docs/v3.4.0/op-guide/security/
https://pcocc.readthedocs.io/en/latest/deps/etcd-production.html
https://github.com/coreos/docs/blob/master/os/generate-self-signed-certificates.md
https://github.com/cloudflare/cfssl

https://github.com/etcd-io/etcd/blob/master/Documentation/op-guide/authentication.md

alias etcdctls="etcdctl --endpoints https://localhost:2379 --cert=/etc/etcd/client.pem --key=/etc/etcd/client-key.pem --cacert=/etc/etcd/ca.pem"

sudo etcdctls endpoint health
sudo etcdctls member list
sudo etcdctls user add root
sudo etcdctls --user root:root user grant-role root root
sudo etcdctls --user root:root get / --prefix --keys-only
